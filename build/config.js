"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var dotenv = __importStar(require("dotenv"));
dotenv.config();
var config = {
    PORT: process.env.PORT || "8080",
    NODE_ENV: process.env.NODE_ENV || 'dev',
    APP_NAME: process.env.APP_NAME,
    MONGO_USER: process.env.MONGO_USER,
    MONGO_PASSWD: process.env.MONGO_PASSWD,
    MONGO_HOST: process.env.MONGO_HOST,
    MONGO_PORT: process.env.MONGO_PORT,
    MONGO_DBNAME: process.env.MONGO_DBNAME,
    PREFIX: process.env.PREFIX,
    WEATHEAR_OPEN_DATA_API: process.env.WEATHEAR_OPEN_DATA_API
};
exports.config = config;
