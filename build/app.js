"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var koa_1 = __importDefault(require("koa"));
var koa_body_1 = __importDefault(require("koa-body"));
var koa_json_1 = __importDefault(require("koa-json"));
var cors_1 = __importDefault(require("@koa/cors"));
var koa_favicon_1 = __importDefault(require("koa-favicon"));
var config_1 = require("./config");
var logger_1 = require("./libs/logger");
var logger_middle_1 = require("./middleware/logger.middle");
var error_middle_1 = require("./middleware/error.middle");
var healthCheck_router_1 = require("./router/healthCheck.router");
var index_1 = require("./router/index");
var notFound_router_1 = require("./router/notFound.router");
var updateWeatherInfo_cron_1 = require("./cronJob/updateWeatherInfo.cron");
var path_1 = __importDefault(require("path"));
var app = new koa_1.default();
var PORT = Number(config_1.config.PORT);
var appName = config_1.config.APP_NAME;
var NODE_ENV = config_1.config.NODE_ENV;
app.use(koa_favicon_1.default(path_1.default.join(__dirname, '/static/favicon.ico')));
// Enbale CORS with default option
app.use(cors_1.default());
// Enable bodyParser with default option
app.use(koa_body_1.default());
app.use(koa_json_1.default());
// Logger middleware -> use console as logger (logging.ts with config)
app.use(logger_middle_1.loggerMiddleFunc());
app.use(error_middle_1.errorMiddleware);
// Setup Router
app.use(healthCheck_router_1.healthCheckRouter.routes());
app.use(index_1.rootRouter.routes());
app.use(notFound_router_1.notFoundRouter.routes());
app.listen(PORT, function () {
    logger_1.logger.info(appName + " for NODE_ENV " + NODE_ENV + ", listening on " + PORT);
});
updateWeatherInfo_cron_1.updateWeatherInfoTask.start();
exports.default = app;
