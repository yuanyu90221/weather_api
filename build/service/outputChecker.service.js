"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ajv_service_1 = require("./ajv.service");
var get_weather_api_json_1 = __importDefault(require("../api_out_schema/get_weather_api.json"));
exports.isAPIOutGetWeatherValid = function (paramObj) {
    return ajv_service_1.validSchema(paramObj, get_weather_api_json_1.default);
};
