"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ajv_1 = __importDefault(require("ajv"));
exports.validSchema = function (paramObj, schema) {
    var ajv = new ajv_1.default({ allErrors: true });
    var valid = ajv.validate(schema, paramObj);
    if (!valid) {
        return [false, ajv.errors];
    }
    else {
        0;
        return [true];
    }
};
