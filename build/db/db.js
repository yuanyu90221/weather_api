"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var logger_1 = require("../libs/logger");
var config_1 = require("../config");
var connectString = "mongodb://" + config_1.config.MONGO_USER + ":" + config_1.config.MONGO_PASSWD + "@" + config_1.config.MONGO_HOST + ":" + config_1.config.MONGO_PORT + "/" + config_1.config.MONGO_DBNAME;
var DBManager = {
    getConnection: function () {
        logger_1.logger.info("connectString :" + connectString);
        return mongoose_1.default.createConnection("" + connectString, { useNewUrlParser: true, useUnifiedTopology: true });
    }
};
exports.DBManager = DBManager;
