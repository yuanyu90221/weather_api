"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var Schema = mongoose_1.default.Schema;
var db_1 = require("./db");
var conn = db_1.DBManager.getConnection();
exports.conn = conn;
var weatherInfoSchema = new Schema({
    _id: {
        type: String
    },
    locationName: {
        type: String,
        default: ""
    },
    value: {
        type: Number,
        default: 0
    },
    measures: {
        type: String,
        default: ""
    },
    dataTime: {
        type: Number,
        default: 0
    },
    lat: {
        type: Number,
        default: 0
    },
    lon: {
        type: Number,
        default: 0
    },
    geocode: {
        type: String,
        default: ""
    }
});
var WeatherInfos = conn.model('WeatherInfos', weatherInfoSchema);
exports.WeatherInfos = WeatherInfos;
