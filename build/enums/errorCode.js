"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ErrorCode;
(function (ErrorCode) {
    ErrorCode["Unknown"] = "010000";
    ErrorCode["NotSupportApiError"] = "010001";
    ErrorCode["GetWeatherApiParameterMissing"] = "010002";
    ErrorCode["GetWeatherApiResponseError"] = "010003";
    ErrorCode["QueryWeatherApiServiceError"] = "010100";
})(ErrorCode = exports.ErrorCode || (exports.ErrorCode = {}));
