"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Logger = /** @class */ (function () {
    function Logger() {
        this.log = function () {
            var data = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                data[_i] = arguments[_i];
            }
            console.log.apply(console, data);
        };
        this.info = function () {
            var data = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                data[_i] = arguments[_i];
            }
            console.info.apply(console, data);
        };
        this.debug = function () {
            var data = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                data[_i] = arguments[_i];
            }
            console.debug.apply(console, data);
        };
        this.error = function (data) {
            console.error(data);
        };
    }
    return Logger;
}());
var logger = new Logger();
exports.logger = logger;
