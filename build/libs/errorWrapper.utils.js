"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var httpException_1 = __importDefault(require("../exception/httpException"));
var errorCode_1 = require("../enums/errorCode");
exports.wrapError = function (e) {
    if (e.name !== "HttpException") {
        var status_1 = e.statusCode ? e.statusCode : 500;
        var message = e.message ? e.message : e.toStrin();
        e = new httpException_1.default(status_1, message, errorCode_1.ErrorCode.Unknown);
    }
    throw e;
};
