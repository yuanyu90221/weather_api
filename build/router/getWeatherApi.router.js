"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var koa_router_1 = __importDefault(require("koa-router"));
var getWeatherApi_controller_1 = require("../controller/getWeatherApi.controller");
var getWeatherApiRouter = new koa_router_1.default();
exports.getWeatherApiRouter = getWeatherApiRouter;
getWeatherApiRouter.get('/weather', getWeatherApi_controller_1.getWeatherApiController);
