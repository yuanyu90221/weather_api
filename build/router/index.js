"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var koa_router_1 = __importDefault(require("koa-router"));
var config_1 = require("../config");
var getWeatherApi_router_1 = require("./getWeatherApi.router");
var apiPrefix = config_1.config.PREFIX ? config_1.config.PREFIX : "";
var rootRouter = new koa_router_1.default({
    prefix: "/" + apiPrefix
});
exports.rootRouter = rootRouter;
rootRouter.use(getWeatherApi_router_1.getWeatherApiRouter.routes());
