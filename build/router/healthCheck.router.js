"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var koa_router_1 = __importDefault(require("koa-router"));
var healthCheckRouter = new koa_router_1.default();
exports.healthCheckRouter = healthCheckRouter;
healthCheckRouter.get('/healthCheck', function (ctx) {
    ctx.status = 200;
    ctx.body = { message: "status well" };
});
