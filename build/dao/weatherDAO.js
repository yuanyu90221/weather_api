"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var weather_infos_1 = require("../db/weather_infos");
var logger_1 = require("../libs/logger");
var updateOne = function (data) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/, new Promise(function (resolve, reject) {
                weather_infos_1.WeatherInfos.findOneAndUpdate({ _id: data._id }, data, { new: true, upsert: true }, function (err, updated) {
                    if (err) {
                        reject(err);
                    }
                    logger_1.logger.info("updated", updated);
                    resolve(updated);
                });
            })];
    });
}); };
exports.updateOne = updateOne;
var insertMany = function (data) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/, new Promise(function (resolve, reject) {
                weather_infos_1.WeatherInfos.insertMany(data, function (error, result) {
                    if (error) {
                        reject(error);
                    }
                    resolve(result);
                });
            })];
    });
}); };
exports.insertMany = insertMany;
var findAll = function (criteria) { return __awaiter(void 0, void 0, void 0, function () {
    var q, limit, offset, condition, finalCondition, matchDataTime, currentDate, searchLen, nextDate;
    return __generator(this, function (_a) {
        q = criteria.q, limit = criteria.limit, offset = criteria.offset;
        logger_1.logger.info(limit, offset);
        finalCondition = {};
        if (q.length !== 0) {
            matchDataTime = /[0-9]{4}-[0-9]{2}-[0-9]{2}/.test(q);
            if (isNaN(Number(q)) == false && q.length < 3) {
                condition = { value: Number(q) };
            }
            else if (!isNaN(Number(q.slice(0, 3))) || matchDataTime) {
                currentDate = new Date(q);
                searchLen = q.length == 4 ? 365 : q.length == 7 ? 30 : 1;
                nextDate = currentDate.setDate(currentDate.getDate() + searchLen);
                condition = { dataTime: { $gte: new Date(q).getTime(), $lte: nextDate } };
            }
            else {
                condition = { locationName: q };
            }
            finalCondition = condition;
        }
        return [2 /*return*/, new Promise(function (resolve, reject) {
                logger_1.logger.info(finalCondition);
                weather_infos_1.WeatherInfos.find(finalCondition, function (error, result) {
                    if (error) {
                        reject(error);
                    }
                    resolve(result);
                }).skip(offset).limit(limit).sort({ _id: 1 });
            })];
    });
}); };
exports.findAll = findAll;
