import {keyValue} from '../interfaces/keyValue.interface';
import {validSchema} from './ajv.service';
import get_weather_api_schema from '../api_out_schema/get_weather_api.json';

export const isAPIOutGetWeatherValid = (paramObj: keyValue) => {
    return validSchema(paramObj, get_weather_api_schema);
};