import Ajv from 'ajv';
import {keyValue} from '../interfaces/keyValue.interface';
export const validSchema = (paramObj: keyValue|[], schema: keyValue)=>{
    const ajv =new Ajv({allErrors: true});
    const valid = ajv.validate(schema, paramObj);
    if (!valid) {
        return [false, ajv.errors];
    } else {0
        return [true]
    }
};