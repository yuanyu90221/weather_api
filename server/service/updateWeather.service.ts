import {logger} from '../libs/logger';
import {doGetRequest} from '../libs/request.util';
import {WeatherInfo} from '../interfaces/weather.interface';
import {updateOne} from '../dao/weatherDAO';
import {conn} from '../db/weather_infos';
import { keyValue } from '../interfaces/keyValue.interface';
import {config} from '../config';

const updateWeatherService = async() => {
    try {
        logger.info('[updateWeatherService] start');
        const uri = config.WEATHEAR_OPEN_DATA_API||"";
        if (uri === "") {
            throw new Error("WEATHER_OPEN_DATE_API env not set");
        }
        const weatherApiRes = await doGetRequest({q:"", offset:0, limit:1000}, uri);
        console.log('result', weatherApiRes);
        const weatherInfoArray:Array<keyValue> = weatherApiRes.result.results;
        let updateInfoArray: Array<Promise<keyValue>> = [];
        weatherInfoArray.forEach(weatherInfo => {
            const updateInfo : WeatherInfo = {
                dataTime: new Date(weatherInfo.dataTime).getTime(),
                measures: weatherInfo.measures,
                value: Number(weatherInfo.value),
                lon: Number(weatherInfo.lon),
                lat: Number(weatherInfo.lat),
                _id: weatherInfo._id,
                geocode: weatherInfo.geocode,
                locationName: weatherInfo.locationName
            };
            updateInfoArray.push(updateOne(updateInfo));  
        });
        let updateResults: Array<keyValue> = await Promise.all(updateInfoArray);
        console.log(updateResults);
    } catch(err) {
        logger.info('[updateWeatherService] error', err);
        await conn.close((closeErr)=>{
            return new Promise((resolve, reject)=>{
                if(closeErr) {
                    reject(closeErr);
                }
                resolve("[updateWeatherService]mongoose connection close success");
            })
        });
    }
};

export {updateWeatherService};