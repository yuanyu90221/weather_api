import { weatherApiGet} from '../interfaces/weather.interface';
import {findAll} from '../dao/weatherDAO';
import {logger} from '../libs/logger';
import HttpException from '../exception/httpException';
import { ErrorCode } from '../enums/errorCode';
export const queryWeatherApiService = async (data: weatherApiGet) => {
    try {
        logger.info(`[queryWeatherApiService] data`, data);
        const result = await findAll(data);
        logger.info(`[queryWeatherApiService] result`, result);
        return result;
    } catch(error) {
        const errMsg: string = error.message ? error.message: error.toString();
        logger.info(`[queryWeatherApiService] error`, errMsg);
        throw new HttpException(500, `[queryWeatherApiService] error ${errMsg}`, ErrorCode.QueryWeatherApiServiceError);
    }   
};