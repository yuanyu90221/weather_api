import {WeatherInfos} from '../db/weather_infos';
import {WeatherInfo, weatherApiGet } from '../interfaces/weather.interface';
import {keyValue} from '../interfaces/keyValue.interface';
import {logger} from '../libs/logger';
const updateOne = async (data: WeatherInfo):Promise<keyValue>=>{
    return new Promise((resolve, reject)=>{
      WeatherInfos.findOneAndUpdate({_id: data._id}, data, {new: true, upsert: true}, (err,updated:keyValue )=>{
        if(err) {
            reject(err);
        }
        logger.info(`updated`, updated);
        resolve(updated);
      });
    });
};
const insertMany = async (data: Array<WeatherInfo>):Promise<keyValue> => {
    return new Promise((resolve, reject) =>{
        WeatherInfos.insertMany(data, (error, result:Array<keyValue>)=>{
            if (error) {
                reject(error);
            }
            resolve(result);
        });
    });
};
const findAll = async(criteria: weatherApiGet):Promise<keyValue> => {
    const {q, limit, offset} = criteria;
    logger.info(limit, offset);
    let condition: keyValue;
    let finalCondition: keyValue | {} = {};
    if (q.length !== 0 ) {
        const matchDataTime  = /[0-9]{4}-[0-9]{2}-[0-9]{2}/.test(q)
        if (isNaN(Number(q))== false && q.length < 3){
        condition = {value:Number(q)}
        } else if (!isNaN(Number( q.slice(0,3)))||matchDataTime) {
            const currentDate = new Date(q);
            const searchLen = q.length == 4? 365: q.length == 7? 30:1;
            const nextDate = currentDate.setDate(currentDate.getDate()+ searchLen);
            condition = {dataTime:{$gte: new Date(q).getTime(),$lte: nextDate }};
        } else {
            condition ={locationName: q};
        }
        finalCondition = condition;
    } 
    return new Promise((resolve, reject)=>{
        logger.info(finalCondition);
        WeatherInfos.find(finalCondition, (error, result: Array<keyValue>)=>{
            if (error) {
                reject(error);
            } 
            resolve(result);
        }).skip(offset).limit(limit).sort({_id:1});
    });
}
export {insertMany, findAll, updateOne}
