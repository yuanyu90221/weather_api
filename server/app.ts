import Koa from 'koa';
import koaBody from 'koa-body';
import json from 'koa-json';
import cors from '@koa/cors';
import favicon from 'koa-favicon';
import {config} from './config';
import {logger} from './libs/logger';
import {loggerMiddleFunc} from './middleware/logger.middle';
import {errorMiddleware} from './middleware/error.middle';
import {healthCheckRouter} from './router/healthCheck.router';
import {rootRouter} from './router/index';
import { notFoundRouter } from './router/notFound.router';
import {updateWeatherInfoTask} from './cronJob/updateWeatherInfo.cron';
import path from 'path'
const app = new Koa();
const PORT =  Number(config.PORT);
const  appName = config.APP_NAME;
const NODE_ENV = config.NODE_ENV;
app.use(favicon(path.join(__dirname,'/static/favicon.ico')));
// Enbale CORS with default option
app.use(cors());
// Enable bodyParser with default option
app.use(koaBody());
app.use(json());
// Logger middleware -> use console as logger (logging.ts with config)
app.use(loggerMiddleFunc());
app.use(errorMiddleware);

// Setup Router
app.use(healthCheckRouter.routes());
app.use(rootRouter.routes());
app.use(notFoundRouter.routes());

app.listen(PORT, () => {
    logger.info(`${appName} for NODE_ENV ${NODE_ENV}, listening on ${PORT}`);
});
updateWeatherInfoTask.start();
export default app;