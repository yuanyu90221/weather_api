import mongoose from 'mongoose';
const {Schema} = mongoose;
import {DBManager} from './db';

const conn = DBManager.getConnection();
const weatherInfoSchema = new Schema({
    _id: {
        type: String
    },
    locationName: {
        type: String,
        default: ""
    },
    value: {
        type: Number,
        default: 0
    },
    measures: {
        type: String,
        default: ""
    },
    dataTime: {
        type: Number,
        default: 0
    },
    lat: {
        type: Number,
        default: 0
    },
    lon: {
        type: Number,
        default: 0
    },
    geocode: {
        type: String,
        default: ""
    }
});
const WeatherInfos = conn.model('WeatherInfos', weatherInfoSchema);
export {WeatherInfos, conn};