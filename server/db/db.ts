import mongoose from 'mongoose';
import {logger} from '../libs/logger';
import {config} from '../config';
const connectString: string = `mongodb://${config.MONGO_USER}:${config.MONGO_PASSWD}@${config.MONGO_HOST}:${config.MONGO_PORT}/${config.MONGO_DBNAME}`;
const  DBManager  = {
    getConnection: ()=> {
        logger.info(`connectString :${connectString}`);
        return mongoose.createConnection(`${connectString}`, {useNewUrlParser: true, useUnifiedTopology: true});
    }
};
export {DBManager};