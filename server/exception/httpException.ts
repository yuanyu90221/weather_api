import { ErrorCode } from "../enums/errorCode";

export default class HttpException extends Error {
  status: number;
  message: string;
  code: ErrorCode;
  constructor(status: number, message: string, code: ErrorCode) {
    super(message);
    this.name = "HttpException";
    this.status = status;
    this.message = message;
    this.code = code;
  }
};