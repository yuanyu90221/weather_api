import {Context} from 'koa';
import {logger} from '../libs/logger';
export const loggerMiddleFunc = () => {
    return async (ctx: Context, next: ()=> Promise<void>) => {
        const start = new Date().getTime();
        await next();
        const ms = (new Date().getTime()) -start;
        let logLevel: string;
       if (ctx.status >= 500) {
           logLevel = "error";
       } else if (ctx.status >= 400) {
           logLevel = "warn";
       } else {
           logLevel = "info"
       }

       const msg: string = `${ctx.method} ${ctx.originalUrl} ${ctx.status} ${ms}ms ${(ctx.body)?'res body: '+JSON.stringify(ctx.body):(ctx.query)?'req.query: '+JSON.stringify(ctx.query):''}`;
       if (ctx.status >= 400) {
        logger.error(`"err.code:"${ctx.state.errorCode} ${msg}`);
      } else {
        logger.info(`${msg}`);
      }
    }
}