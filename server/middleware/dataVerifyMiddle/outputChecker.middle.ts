import {Context} from 'koa';
import {isAPIOutGetWeatherValid} from '../../service/outputChecker.service';
import HttpException from '../../exception/httpException';
import {ErrorCode} from '../../enums/errorCode';
import {logger} from '../../libs/logger';
import { wrapError } from '../../libs/errorWrapper.utils';

 const validAPIOutGetWeatherApi = async(ctx:Context, next:() => Promise<any>) =>{
    try {
        let {data} = ctx.state;
        let {limit, offset} = ctx.state.inputParams;
        logger.info(`[${ctx.request.path}][${ctx.request.method}] validAPIOutGetWeatherApi`, data);
         const valid =isAPIOutGetWeatherValid(data);
         if (!valid[0]) {
             const errMsg: string = (valid[1])? JSON.stringify(valid[1]):"";
             logger.error(`[${ctx.request.path}][${ctx.request.method}] ${errMsg}`);
             throw new HttpException(400, `[${ctx.request.path}][${ctx.request.method}] api output wrong, please contact us`, ErrorCode.GetWeatherApiResponseError);
         }
         ctx.status = 200;
         ctx.body = {result: {results: data, limit, offset, count: data.length}}
    } catch(err) {
        wrapError(err);
    }
}
export {validAPIOutGetWeatherApi}
