import {Context} from 'koa';
import {isGetWeatherValid} from '../../service/inputChecker.service';
import HttpException from '../../exception/httpException';
import {ErrorCode} from '../../enums/errorCode';
import {logger} from '../../libs/logger';
import { wrapError } from '../../libs/errorWrapper.utils';
const validGetWeatherApi = async (ctx: Context, next:() => Promise<any>) =>{
    try {
        let {q, limit, offset} = ctx.query;
        q = q? q : "";
        limit = limit ? limit : 1000;
        offset = offset? offset : 0;
        const inputParams = {q, limit: Number(limit), offset: Number(offset)};
        logger.info('inputParams', inputParams);
        ctx.state.inputParams = inputParams;
        const valid = isGetWeatherValid({...inputParams});
        if (!valid[0]) {
            const errorMsg: string = (valid[1])? JSON.stringify(valid[1]):"";
            throw new HttpException(400, `[${ctx.request.path}][${ctx.request.method}] api input parameter missing, please check input ${errorMsg}`, ErrorCode.GetWeatherApiParameterMissing);
        }
        await next();
    } catch(err){
        wrapError(err);
    }
}
export {validGetWeatherApi}