import * as Koa from 'koa';
import { ErrorCode } from '../enums/errorCode';
export const errorMiddleware = async(ctx: Koa.Context, next: ()=> Promise<any>) =>{
  try {
    await next();
  } catch(error) {
    const status = error.status || 500;
    const message = error.message || 'something went wrong';
    const errCode = error.code ? error.code: ErrorCode.Unknown;
    ctx.status = status;
    ctx.body = {message, errCode};
    ctx.state.errorCode = error.code ? error.code: ErrorCode.Unknown;
  }
};