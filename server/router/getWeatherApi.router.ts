import Router from 'koa-router';
import {getWeatherApiController} from '../controller/getWeatherApi.controller';
const getWeatherApiRouter = new Router();
getWeatherApiRouter.get('/weather', getWeatherApiController);
export {getWeatherApiRouter}