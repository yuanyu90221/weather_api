import Router from 'koa-router';
import HttpException from '../exception/httpException';
import { Context } from 'koa';
import { ErrorCode } from '../enums/errorCode';
const notFoundRouter = new Router(); 
notFoundRouter.all("*", async(ctx: Context, next:()=>Promise<void>)=>{
  throw new HttpException(404, `${ctx.request.path} not supported`, ErrorCode.NotSupportApiError);
});
export {notFoundRouter};