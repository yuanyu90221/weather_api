import Router from 'koa-router';
import {config} from '../config';
import {getWeatherApiRouter} from './getWeatherApi.router';
const apiPrefix = config.PREFIX? config.PREFIX: "";
const rootRouter = new Router({
    prefix: `/${apiPrefix}`
});
rootRouter.use(getWeatherApiRouter.routes());
export {rootRouter};