import Router from 'koa-router';
import {Context} from 'koa';
const healthCheckRouter = new Router();
healthCheckRouter.get('/healthCheck', (ctx: Context)=>{
    ctx.status = 200;
    ctx.body = {message: "status well"}
});
export {healthCheckRouter};