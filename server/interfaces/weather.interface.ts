export interface weatherApiGet {
    q: string;
    limit: number;
    offset: number;
}

export interface WeatherInfo {
    _id: Number;
    locationName: String;
    value: Number,
    measures: String;
    dataTime: Number;
    lon: Number;
    lat: Number;
    geocode: String;
}