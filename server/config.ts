import * as dotenv from 'dotenv';
dotenv.config()

const config = {
    PORT: process.env.PORT || "8080",
    NODE_ENV: process.env.NODE_ENV || 'dev',
    APP_NAME: process.env.APP_NAME,
    MONGO_USER: process.env.MONGO_USER,
    MONGO_PASSWD: process.env.MONGO_PASSWD,
    MONGO_HOST: process.env.MONGO_HOST,
    MONGO_PORT: process.env.MONGO_PORT,
    MONGO_DBNAME: process.env.MONGO_DBNAME,
    PREFIX: process.env.PREFIX,
    WEATHEAR_OPEN_DATA_API: process.env.WEATHEAR_OPEN_DATA_API
};

export {config}