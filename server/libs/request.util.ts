import request from 'request';
import {logger} from './logger';
import {weatherApiGet} from '../interfaces/weather.interface';
import {keyValue} from '../interfaces/keyValue.interface';
const doGetRequest = async(data: weatherApiGet, url: string):Promise<keyValue>=>{
    logger.info(`[doGetRequest] data`, data);
    logger.info(`[doGetRequest] url`, url);
    return new Promise((resolve, reject)=>{
        request({
            method: 'GET',
            uri: `${url}&q=${data.q}&limit=${data.limit}&offset=${data.offset}`,
            headers: {
                'Content-type': 'application/json'
            },
            json: true
        }, (error, response, result)=>{
            if (error) {
                reject(error);
            } 
            resolve(result);
        });
    });
};

export {doGetRequest};