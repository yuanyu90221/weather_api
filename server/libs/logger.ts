class Logger {
    log = (...data: any) => {
        console.log(...data);
    }
    info = (...data: any) => {
        console.info(...data);
    }
    debug = (...data: any) => {
        console.debug(...data);
    }
    error = (data: string) => {
        console.error(data);
    }
}

const logger = new Logger();
export {logger};