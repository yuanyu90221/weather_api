import HttpException from '../exception/httpException';
import {ErrorCode} from '../enums/errorCode';

export const wrapError = (e: any) => {
    if (e.name !== "HttpException") {
        const status = e.statusCode? e.statusCode: 500;
        const message = e.message? e.message: e.toStrin();
        e = new HttpException(status, message, ErrorCode.Unknown);
    }
    throw e;
};