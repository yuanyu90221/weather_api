import * as cron from 'node-cron';
import {updateWeatherService} from '../service/updateWeather.service';
import {logger} from '../libs/logger';
const updateWeatherInfoTask = cron.schedule('*/59 * * * *', async()=>{
    try {
        logger.info('[updateWeatherInfoTask] start every 1 hours');
        await updateWeatherService();
    } catch(error) {
        logger.info('[updateWeatherInfoTask]', error);
    }
},{scheduled:true});

export {updateWeatherInfoTask};