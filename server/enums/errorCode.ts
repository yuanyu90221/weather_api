export enum ErrorCode {
    Unknown="010000",
    NotSupportApiError="010001",
    GetWeatherApiParameterMissing="010002",
    GetWeatherApiResponseError= "010003",
    QueryWeatherApiServiceError="010100"
  }