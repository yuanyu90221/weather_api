import compose from 'koa-compose';
import {Context} from 'koa';
import {validGetWeatherApi} from '../middleware/dataVerifyMiddle/inputChecker.middle';
import {validAPIOutGetWeatherApi}  from '../middleware/dataVerifyMiddle/outputChecker.middle';
import {queryWeatherApiService} from '../service/queryWeatherApi.service';
import { logger } from '../libs/logger';
import { weatherApiGet } from '../interfaces/weather.interface';
import { wrapError } from '../libs/errorWrapper.utils';

const getWeatherApiController = compose([
    validGetWeatherApi, async(ctx:Context, next:()=>Promise<any>) => {
        try {
            const {inputParams} = ctx.state;
            logger.info(`[${ctx.request.path}][${ctx.request.method}][getWeatherApiControoler]`,  inputParams);
            const searchObj:weatherApiGet = {
                q: inputParams.q,
                limit: inputParams.limit,
                offset: inputParams.offset
            };
            const data =  await queryWeatherApiService(searchObj);
            ctx.state.data =  data;
            await next();
        } catch (error) {
            logger.info(`[${ctx.request.path}][${ctx.request.method}][getWeatherApiControoler]`,  error);
            wrapError(error);
        }
    }
,validAPIOutGetWeatherApi ]);

export {getWeatherApiController};