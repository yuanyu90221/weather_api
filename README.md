# weather_api

這個server 是用來存取

[台北市每日氣溫資料開放資料](https://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=1f1aaba5-616a-4a33-867d-878142cac5c4)

##  introduction

目前由於 資料便於檢查應此使用 typescript 來撰寫api server

koa 為一個適合 middleware api開發的框架

所以這個api的設計主要是由middleware 對資料做驗證 整理

## 結構

 1 middleware :  1 資料檢查: ajv 對前端丟到後端的資料做 在處理前作 檢查
                                  2 error handler 對錯誤做 logger還有分類
                                  3 logger 每個request 

 2 router: 用來分配 api 的路徑對應 controller 

3 controller: 程式主流程所在
 
4 service: 底層服務實做 比如 對DB做存取 這邊使用 mongoose 存取mongodb, node-cron 實做定期更新db

5 libs 一般常用的library比如 request

## Notice 在使用這個程式需要先設定好 .env的各項參數

```javascript===
PORT=
APP_NAME=
NODE_ENV=
MONGO_USER=
MONGO_PASSWD=
MONGO_HOST=
MONGO_PORT=
MONGO_DBNAME=
PREFIX=
WEATHEAR_OPEN_DATA_API=
```

| 名稱  |  作用|
| -          |    -     |
|MONGO_USER|MONGODB user name                |
|MONGO_PASSWD| MONGODB password           |  
|MONGO_HOST      | MONGODB hostname          |
|MONGO_DBNAME      | MONGO DB名稱               |
|MONGO_PORT      | MONGODB PORT                   |
|PORT | server 的 port number                                 |
|APP_NAME| log顯示的app 名稱                              |
|PREFIX         | RESTFUL api的prefix                        |
|WEATHEAR_OPEN_DATA_API open_data_api位址|