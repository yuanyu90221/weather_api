import {logger} from '../server/libs/logger';
import {doGetRequest} from '../server/libs/request.util';
import * as dotenv from'dotenv';
dotenv.config();
import {WeatherInfo} from '../server/interfaces/weather.interface';
import {updateOne} from '../server/dao/weatherDAO';
import {conn} from '../server/db/weather_infos';
import { keyValue } from '../server/interfaces/keyValue.interface';

test('update_test', async()=>{
    try {
        const uri = "https://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=1f1aaba5-616a-4a33-867d-878142cac5c4";
        const result = await doGetRequest({q:"", offset:0, limit:1000}, uri);
        console.log('result', result);
        const allResults:Array<keyValue> = result.result.results;
        let updateArray: Array<Promise<keyValue>> = [];
        allResults.forEach(finalResult => {
            const updateObj : WeatherInfo = {
                dataTime: new Date(finalResult.dataTime).getTime(),
                measures: finalResult.measures,
                value: Number(finalResult.value),
                lon: Number(finalResult.lon),
                lat: Number(finalResult.lat),
                _id: finalResult._id,
                geocode: finalResult.geocode,
                locationName: finalResult.locationName
            };
            updateArray.push(updateOne(updateObj));  
        });
        let updateResults: Array<keyValue> = await Promise.all(updateArray);
        await conn.close((error)=>{
            return new Promise((resolve, reject)=>{
                if (error) {
                  reject(error);
                }
                resolve("");
            });
        });
        console.log(updateResults);
    } catch (err) {
        logger.info('err', err);
    }
}, 50000);