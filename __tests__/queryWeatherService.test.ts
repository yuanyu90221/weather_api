import {queryWeatherApiService} from '../server/service/queryWeatherApi.service';
import {logger} from '../server/libs/logger';
import { weatherApiGet } from '../server/interfaces/weather.interface';
test('queryWeatherApiService', async()=>{
    try {
        logger.info(`[queryWeatherApiSerivce]`);
        const searchObj: weatherApiGet = { q: '2020-04-31', offset:0, limit:1};
        const result = await queryWeatherApiService(searchObj);
        expect(Array.isArray(result)).toBe(true)
       
    } catch (error){
        logger.info('[queryWeatherApiService] error', error);
    }
});