import {logger} from '../server/libs/logger';
// import {doGetRequest} from '../server/libs/request.util';
import * as dotenv from'dotenv';
dotenv.config();
import {WeatherInfo, weatherApiGet} from '../server/interfaces/weather.interface';
import {findAll} from '../server/dao/weatherDAO';
import {conn} from '../server/db/weather_infos';
import { keyValue } from '../server/interfaces/keyValue.interface';

test('query_test', async()=>{
    try {
        const searchObj: weatherApiGet = {
            q : "",
            offset: 0,
            limit: 1000
        };
        const result = await findAll(searchObj);
        await conn.close((err1)=>{
            return new Promise((resolve, reject)=>{
                if (err1) reject(err1)
                resolve("");
            })
        });
        console.log('result', result);
    } catch(err){
        logger.info('err', err);
    }
})