import {logger} from '../server/libs/logger';
import {doGetRequest} from '../server/libs/request.util';

test('doGetRequest_test', async()=>{
    try {
        const uri = "https://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=1f1aaba5-616a-4a33-867d-878142cac5c4";
        const result = await doGetRequest({q:"", offset:0, limit:1000}, uri);
        // logger.info(`doGetRequest_test result`, result);
        expect(result).toHaveProperty("result");
        expect(result["result"]).toHaveProperty("results");
        expect(result["result"]).toHaveProperty("count");
    } catch (error) {
        logger.info(`doGetRequest_test error`, error);
    }
});