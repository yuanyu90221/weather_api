import {logger} from '../server/libs/logger';
import {doGetRequest} from '../server/libs/request.util';
import * as dotenv from'dotenv';
dotenv.config();
import {WeatherInfo} from '../server/interfaces/weather.interface';
import {insertMany} from '../server/dao/weatherDAO';
import {conn} from '../server/db/weather_infos';
import { keyValue } from '../server/interfaces/keyValue.interface';
test('test_insert', async()=>{
    try {
        const uri = "https://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=1f1aaba5-616a-4a33-867d-878142cac5c4";
        const result = await doGetRequest({q:"", offset:0, limit:1000}, uri);
        const allResults:Array<keyValue> = result.result.results;
        let insertArray: Array<WeatherInfo> = [];
        allResults.forEach(finalResult => {
                const insertObj : WeatherInfo = {
                dataTime: new Date(finalResult.dataTime).getTime(),
                measures: finalResult.measures,
                value: Number(finalResult.value),
                lon: Number(finalResult.lon),
                lat: Number(finalResult.lat),
                _id: finalResult._id,
                geocode: finalResult.geocode,
                locationName: finalResult.locationName
            };
            insertArray.push(insertObj);
        })
        // const finalResult = ;
        
        try {
          const arr = await insertMany(insertArray);
          await conn.close((err1)=>{
            return new Promise((resolve, reject)=>{
                if (err1) {
                    reject(err1);
                }
                resolve('');
            })
          })
          logger.info(arr);
        } catch (error) {
          logger.info('error', error);
        }
    } catch (err){
        logger.info('test_insert error', err);
    }
})